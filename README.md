# pdf-converter

A bash script that helps the user to convert pfd documents to other formats like odt doc, docx, md etc.

## Dependencies

The script uses the `pdftohtml`  & `pandoc` commands.
To install them, open a terminal and run:

    sudo apt install pdftohtml pandoc

## Install

To install download the pdf-converter folder by clicking on the __Download button__ at [https://gitlab.com/christosangel/pdf-converter](https://gitlab.com/christosangel/pdf-converter).
After that, unzip the folder, change directory inside this folder, and make the pdf-converter.sh executable by running:

    chmod +x pdf-converter.sh

Then, to run the script:

    ./pdf-converter.sh

## Run

Initially you are prompted by a file dialog to enter the pdf file you wish to convert.

After that, the user will be prompted to select the output file format.

After a feaw seconds (depending on the size of the file, and the hardware processing capability), in the same directory with the input pdf file, will appear the output file with the selected format.


